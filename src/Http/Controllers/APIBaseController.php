<?php 
namespace Sunnydevbox\TWCore\Http\Controllers;

use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use Illuminate\Api\Http\Response;

abstract class APIBaseController implements APIInterface
{
	use Helpers;

/*
	return $this->response->noContent();
	return $this->response->created();
	return $this->response->created($location);

	// A generic error with custom message and status code.
	return $this->response->error('This is an error.', 404);

	// A not found error with an optional message as the first parameter.
	return $this->response->errorNotFound();

	// A bad request error with an optional message as the first parameter.
	return $this->response->errorBadRequest();

	// A forbidden error with an optional message as the first parameter.
	return $this->response->errorForbidden();

	// An internal error with an optional message as the first parameter.
	return $this->response->errorInternal();

	// An unauthorized error with an optional message as the first parameter.
	return $this->response->errorUnauthorized();
*/
	public function index(Request $request)
	{	
		$limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);
		
		if ($limit == 0) {
			$result = $this->repository->all();

			return $this->response()
					->collection($result, $this->transformer)
					->withHeader('Content-Range', $result->count());
		} else {
			$result = $this->repository->paginate($limit);

			return $this->response
				->paginator($result, $this->transformer)
				->withHeader('Content-Range', $result->total());
		}
	}

	public function show($id)
	{
		$result = $this->repository->find($id);

		if (!$result && isset($this->return_messages)) {
			return $this->response->error($this->return_messages['object_not_found'], 404);
		}

		return $this->response->item($result, $this->transformer);
	}

	public function store(Request $request)
	{
		 try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}
			
		    $result = $this->repository->create( $request->all() );

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function destroy($id)
	{
		$obj = $this->repository->delete($id);
		
		if (!$obj && isset($this->return_messages)) {
			return $this->response->error($this->return_messages['object_not_found'], 404);
		}

		return $this->response->noContent();

		if ($obj->delete()) {
			return $this->response->noContent();
		} else {
			return $this->response->error('could_not_delete_book', 500);
		}
	}

	public function update(Request $request, $id)
	{
		try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}
           
			$result = $this->repository->update($request->all(), $id);

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	/** DEPRECATED */
	private function find($id)
	{
		return $this->repository->find($id);
		// return $this->repository->makeModel()->find($id);
	}
}
