<?php
namespace Sunnydevbox\TWCore\Http\Controllers;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class ActivityLogController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWCore\Repositories\ActivityLogRepository $repository, 
		\Sunnydevbox\TWCore\Validators\ActivityLogValidator $validator,
		\Sunnydevbox\TWCore\Transformers\ActivityLogTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
    }
}