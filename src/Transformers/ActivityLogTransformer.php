<?php
namespace Sunnydevbox\TWCore\Transformers;

use League\Fractal\TransformerAbstract;

class ActivityLogTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return [
            'id'        	=> (int) $obj->id,
            'name'        	=> $obj->log_name,
            'description' 	=> $obj->description,
            'created_at'	=> date('Y-m-d H:i:s', strtotime($obj->created_at)),
        ];
    }
}