<?php

namespace Sunnydevbox\TWCore;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

use Sunnydevbox\TWCore\BaseServiceProviderInterface;

class BaseServiceProvider extends ServiceProvider implements BaseServiceProviderInterface
{
    protected $toPublishConfig = [];
    protected $routes = [];
    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        //$this->publishes([__DIR__.'/../config/tw-core.php' => config_path('tw-core.php')], 'config');
        //$this->loadRoutesFrom(__DIR__.'/../routes/api.php');

        $this->publishes($this->toPublishConfig(), 'config');

        foreach($this->loadRoutes() as $route) {
            $this->loadRoutesFrom($route);
        }

        foreach($this->mergeConfig() as $k => $v) {
            $this->mergeConfigFrom($k, $v);
        }

        foreach($this->loadViews() as $k => $v) {
            $this->loadViewsFrom($k, $v);
        }
    }   

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerCommands();
        $this->registerPublishers();
    }

    /** 
     * OVERRIDE 
     */
    public function toPublishConfig()
    {
        return [];
    }

    /** 
     * OVERRIDE 
     */
    public function loadRoutes()
    {
        return [
            __DIR__.'/../routes/api.php'
        ];
    }

    /** 
     * OVERRIDE 
     */
    public function mergeConfig()
    {
        return [];
    }

    public function loadViews()
    {
        return [];
    }
    
    public function registerProviders(){}
    public function registerCommands(){}
    public function registerPublishers(){}
}
