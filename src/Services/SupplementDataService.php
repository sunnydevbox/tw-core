<?php
namespace Sunnydevbox\TWCore\Services;

class SupplementDataService
{
    public function d($d)
    {
        switch($d) {
            // case 'roles':
            //     return $this->rpoRole->all()->toArray();
            // break;

            // case 'permissions':
            //     return $this->rpoPaymerpoPermissionntTerm->all()->toArray();
            // break;

            default:
                return [];
            break;
        }
    }


    
    public function _suppData($request)
    {
        $data = $request->get('data', []); 

        $datum = explode(';', strtolower($data));
        $return = [];
        foreach($datum as $d) {
            $d = strtolower($d);

            $return[$d] = $this->d($d);
        }

        return $return;
    }

    public function __construct(
        // \Sunnydevbox\TWInventory\Repositories\DeliveryMethod\DeliveryMethodRepository $rpoDeliveryMethod,
        // \Sunnydevbox\TWInventory\Repositories\PaymentTerm\PaymentTermRepository $rpoPaymentTerm,
        // \Sunnydevbox\TWInventory\Repositories\Location\LocationRepository $rpoLocation,
        // \Sunnydevbox\TWInventory\Repositories\Supplier\SupplierRepository $rpoSupplier,
        // \Sunnydevbox\TWInventory\Repositories\Manufacturer\ManufacturerRepository $rpoManufacturer,
        // \Sunnydevbox\TWInventory\Repositories\Metric\MetricRepository $rpoMetric,
        // \Sunnydevbox\TWInventory\Repositories\Category\CategoryRepository $rpoCategory
    ) {

        // $this->rpoDeliveryMethod = $rpoDeliveryMethod;
        // $this->rpoPaymentTerm = $rpoPaymentTerm;
        // $this->rpoLocation = $rpoLocation;
        // $this->rpoSupplier = $rpoSupplier;
        // $this->rpoMetric = $rpoMetric;
        // $this->rpoManufacturer = $rpoManufacturer;
        // $this->rpoCategory = $rpoCategory;
    }
}