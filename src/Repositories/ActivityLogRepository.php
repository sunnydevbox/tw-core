<?php
namespace Sunnydevbox\TWCore\Repositories;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;


class ActivityLogRepository extends TWBaseRepository
{
	public function model()
	{
		// return config('laravel-activitylog.activity_model');
		return config('tw-core.models.activity_log');
	}
}