<?php

namespace Sunnydevbox\TWCore\Repositories;

use Phoenix\EloquentMeta\MetaTrait;

trait TWMetaTrait{
    
    use MetaTrait;
    
    public function metaFields()
    {
        return $this->meta;
    }

    public function saveMeta($metas)
    {
        $collection = collect($this->metaFields());
        
        foreach($metas as $key => $value) {

            if ($collection->contains($key)) {

                $this->updateMeta($key, $value);
                
            }
        }

        return $this;
    }
}