<?php

namespace Sunnydevbox\TWCore\Repositories;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

class TWBaseRepository extends BaseRepository implements CacheableInterface
{
    

    protected $cacheMinutes = 90;
    //protected $cacheOnly = ['all', ];
    //or
    //protected $cacheExcept = ['all', ];

    use CacheableRepository;
    
    public function boot()
    {   
        $this->pushCriteria(app('\Sunnydevbox\TWCore\Criteria\RequestCriteria'));
    }

    public function model()
    {
        return null;
    }

    
    public function update(array $attributes, $id)
    {
        $result = parent::update($attributes, $id);
        
        $result = $this->saveMeta($result, $attributes);

        return $result;
    }

    public function create(array $attributes)
    {
        $result = parent::create($attributes);

        $result = $this->saveMeta($result, $attributes);

        return $result;
    }

    public function saveMeta($model, $attributes) 
    {
        
        if (method_exists($model, 'saveMeta') && count($model->metaFields())) {

            $metaFields = $model->metaFields();

            if(is_a($metaFields, 'Illuminate\Database\Eloquent\Collection')) {
                $metaFields = $model->metaFields()->all();
            }
            
            $toMeta = collect($attributes)
                        // ->only($model->metaFields())
                        ->only($metaFields)
                        ->all();

            $model = $model->saveMeta($toMeta);
        }

        return $model;
    }
}