<?php
namespace Sunnydevbox\TWCore\Traits;

use Bnb\Laravel\Attachments\HasAttachment;

trait FileAttachmentTrait
{
	use HasAttachment;
}