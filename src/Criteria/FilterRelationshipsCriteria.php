<?php

namespace Sunnydevbox\TWCore\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class FilterRelationshipsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /*
         * Format:
         * &filterRelationships=relationship.columnname:value:comparisonOperator:logicalOperator&filterJoin=OR
         *
         * Example:
         * &filterRelationships=tags.slug:ocean|waterfalls:comparisonOperator:logicalOperator&filterJoin=OR
         *
         * Params;
         * relationship.columnname  (REQUIRED)
         * value                    (REQUIRED) optional multiple values separate by | (pipe)
         * comparisonOperator       (OPTIONAL) default: =
         * logicaloperator          (OPTIONAL) default: AND
         *
         * &filterJoin This is usefule if you specify multiple filterRelationships
         */
        

        //searchJoinFilter
        $searchJoinFilter = (request()->get('searchJoinFilter')) ? strtolower(request()->get('searchJoinFilter')) : 'and';

        $where = ($searchJoinFilter == 'and') ? 'where' : 'orWhere';


        $model->{$where}(function($model)
        {
            if (request()->get('filterRelationships')) {

                $filterRelationships = explode(';', request()->get('filterRelationships'));
                $filterJoin = strtolower(request()->get('filterJoin'));
                $modelForceAndWhere = $filterJoin ? $filterJoin : 'and';
                $isFirstField = true;
                
                foreach($filterRelationships as $filterRelationship) {
                    $pair = explode(':', $filterRelationship);
                    
                    if (isset($pair[0]) && strlen($pair[0])) {
                        $field = $pair[0];
                        $values = explode('|', $pair[1]);

                        $originalCondition = trim(strtolower((isset($pair[2]) ? $pair[2] : '=')));
                        $logicalOperator = trim(strtolower((isset($pair[3]) ? $pair[3] : 'or')));
                        
                        $relation = null;
                        if(stripos($field, '.')) {
                            $explode = explode('.', $field);
                            $field = array_pop($explode);
                            $relation = implode('.', $explode);
                        }


                        if(!is_null($relation) && !is_null($field)) {
                            $conditioner = ($modelForceAndWhere == 'and')? 'whereHas' : 'orWhereHas';
                            
                            $model->{$conditioner}($relation, function($query) use($field, $originalCondition, $values, $isFirstField, $logicalOperator) {
                                foreach($values as $value) {

                                    switch($originalCondition) {
                                        case 'like%':
                                            $value = "{$value}%";
                                            $condition = 'like';
                                            break;

                                        case '%like':
                                            $value = "%{$value}";
                                            $condition = 'like';
                                            break;

                                        default:
                                            $value = ($originalCondition == "like" || $originalCondition == "ilike") ? "%{$value}%" : $value;
                                            $condition = 'like';
                                        break;
                                    }

                                    if ($isFirstField || $logicalOperator == 'and') {
                                        $query->where($field,$condition,$value);
                                    } else {
                                        $query->orWhere($field,$condition,$value);
                                    }

                                    $isFirstField = false;
                                }

                            });
                        }
                    }
                }   

            } 
        });
        return $model;
    }
}
