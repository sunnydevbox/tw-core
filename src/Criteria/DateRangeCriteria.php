<?php

namespace Sunnydevbox\TWCore\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use \Carbon\Carbon;
use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class DateRangeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // dateRange=columnname:2016-01-01:>=|2017-12-30
        // dateRange=columnname;2016-01-01:>=|2017-12-30
        $dateRange = request()->get('dateRange');

        // DATE RANGE
        if ($dateRange) {
            $dateRanges = explode('|', $dateRange);
            
            if (isset($dateRanges[0])) {
                $operator = '>=';
                $valueOperator = explode(';', $dateRanges[0]);
                $column = $valueOperator[0];
                $value = $valueOperator[1];


                if (isset($valueOperator[2]) && strlen($valueOperator[2]) > 0) {
                    $operator = $valueOperator[2];
                }

                
                if ($parts = explode('.', $column)) {
                    $last = array_pop($parts);
                    $parts = array(implode('_', $parts), $last);
                    $relationship = $parts[0];
                    $column = $parts[1];

                    $model = $model->whereHas($relationship, function($query) use ($column, $operator, $value) {
                        $query->whereDate($column, $operator, $value);
                    });

                } else {
                    $model = $model->where($column, $operator, $value);
                }
            }


            if (isset($dateRanges[1])) {
                $operator = '<=';
                $valueOperator = explode(';', $dateRanges[1]);
                $column = $valueOperator[0];
                $value = $valueOperator[1];

                if (isset($valueOperator[2]) && strlen($valueOperator[2]) > 0) {
                    $operator = $valueOperator[2];
                }

                if ($parts = explode('.', $column)) {
                    $last = array_pop($parts);
                    $parts = array(implode('_', $parts), $last);
                    $relationship = $parts[0];
                    $column = $parts[1];

                    $model = $model->whereHas($relationship, function($query) use ($column, $operator, $value) {
                        $query->whereDate($column, $operator, $value);
                    });

                } else {
                    $model = $model->where($column, $operator, $value);
                }
            }
        }   
        
        return $model;
    }
}
