<?php

namespace Sunnydevbox\TWCore\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class SearchRelationshipsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /*
         * Example:
         * &searchRelationships=tags.name:like;tags.slug:like;entities.name:like
         */
        


        if (request()->get('searchRelationships')) {

            $search = $mainSearch = request()->get('search');
            $searchRelationships = explode(';', request()->get('searchRelationships'));
            $searchJoin = request()->get('searchJoin');
            $modelForceAndWhere = strtolower($searchJoin) === 'and';
            $isFirstField = true;

           
            foreach($searchRelationships as $searchRelationship) {
                $pair = explode(':', $searchRelationship);
                $field = $pair[0];

                $condition = trim(strtolower((isset($pair[1]) ? $pair[1] : '=')));
                $search = isset($pair[2]) ? $pair[2] : $mainSearch;

                //$value = ($condition == "like" || $condition == "ilike") ? "%{$search}%" : $search;

                switch($condition) {
                    case 'like%':
                        $value = "{$search}%";
                        $condition = 'like';
                        break;

                    case '%like':
                        $value = "%{$search}";
                        $condition = 'like';
                        break;

                    case 'like':
                        $value = "%{$search}%";
                        $condition = 'like';
                        break;

                    default:
                        $value = $search; // ($condition == "like" || $condition == "ilike") ? "%{$search}%" : $value;
                    break;
                }

                $relation = null;
                if(stripos($field, '.')) {
                    $explode = explode('.', $field);
                    $field = array_pop($explode);
                    $relation = implode('.', $explode);
                }
                
                if ( $modelForceAndWhere && ($isFirstField || $modelForceAndWhere) ) {
                    if (!is_null($value)) {
                        if(!is_null($relation) && !is_null($field)) {

                            $model->whereHas($relation, function($query) use($field,$condition,$value) {
                                $query->where($field,$condition,$value);
                            });
                        }

                        
                        $isFirstField = false;
                    }
                } else {
                    if (!is_null($value)) {
                        if(!is_null($relation) && !is_null($field)) {
                            $model->orWhereHas($relation, function($query) use($field,$condition,$value) {
                                $query->where($field,$condition,$value);
                            });
                        }
                    }
                }
            }   

        }
        // var_dump($model->getBindings());
        // dd($model->toSql());
        return $model;
    }
}
