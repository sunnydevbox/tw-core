<?php

namespace Sunnydevbox\TWCore\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class WhereNullCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /*
         * Add "IS NULL" operator capability to searchFields
         */
        if (request()->get('searchFields')) {
            $searchFields = explode(';', request()->get('searchFields'));

            foreach($searchFields as $searchField) {
                $pair = explode(':', $searchField);

                if (count($pair) == 2 && strtoupper($pair[1]) == 'ISNULL') {
                    
                    $model = $model->whereNull($pair[0]);
                }
            }   
        }
        
        return $model;
    }
}
