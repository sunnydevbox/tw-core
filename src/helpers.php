<?php

if(!function_exists('_dd'))
{
	function _dd(...$args)
	{
		header("HTTP/1.0 500 Internal Server Error");
		echo 'Start of dd() ...';
		call_user_func_array('dd', $args);
	}
}
