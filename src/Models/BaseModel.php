<?php

namespace Sunnydevbox\TWCore\Models;
use Spatie\Activitylog\Traits\LogsActivity;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	// use LogsActivity;
	protected static $logOnlyDirty = true;
}