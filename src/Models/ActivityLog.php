<?php
namespace Sunnydevbox\TWCore\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
	public function getTable() 
	{
		return config('tw-core.tables.activity_log');
	}
}