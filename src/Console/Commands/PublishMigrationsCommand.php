<?php

namespace Sunnydevbox\TWCore\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishMigrationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twcore:publish-migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWCore migrations files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Copying ACTIVITY LOGGER migrations');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Spatie\Activitylog\ActivitylogServiceProvider',
            '--tag'        => 'migrations',
        ]);
        $this->info('...DONE');

        $this->info('Copying ELOQUENT META migrations');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Phoenix\EloquentMeta\ServiceProvider',
            '--tag'        => 'migrations',
        ]);
        $this->info('...DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
