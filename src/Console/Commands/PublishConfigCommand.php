<?php

namespace Sunnydevbox\TWCore\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twcore:publish-config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWCore config files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $this->info('Publishing ACTIVITY LOGGER config');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Spatie\Activitylog\ActivitylogServiceProvider',
            '--tag'        => 'config',
        ]);
        $this->info('...DONE');


        $this->info('Publishing PRETTUS REPOSITORY config');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Prettus\Repository\Providers\RepositoryServiceProvider',
            '--tag'        => 'config',
        ]);
        $this->info('...DONE');


        $this->info('Publishing DINGO API REPOSITORY config');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Dingo\Api\Provider\LaravelServiceProvider',
        ]);
        $this->info('...DONE');

        $this->info('Publishing NZTIM config');
        Artisan::call('vendor:publish', [
            '--provider'    => 'NZTim\Logger\LoggerServiceProvider',
        ]);
        $this->info('...DONE');



        $this->info('Publishing TYMON JWT REPOSITORY config');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Tymon\JWTAuth\Providers\JWTAuthServiceProvider',
        ]);
        $this->info('...DONE');

        $this->info('Publishing TYMON JWT REPOSITORY generating secret');
        Artisan::call('jwt:generate');
        $this->info('...DONE');

        $this->info('Publishing SWAGGERVEL vendor files');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Jlapp\Swaggervel\SwaggervelServiceProvider',
        ]);
        $this->info('...DONE');

        $this->info('Bnb\Laravel\Attachments migrating');
        Artisan::call('vendor:publish', [
            '--provider'    => 'Bnb\Laravel\Attachments\AttachmentsServiceProvider',
        ]);
        $this->info('...DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
