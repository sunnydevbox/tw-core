<?php

namespace Sunnydevbox\TWCore\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class OptimizeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twcore:optimize {--clear}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'TWCore optimization';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        //dd($this->option('clear'));
        /*
            
            php artisan config:cache
            php artisan route:cache
            php artisan optimize --force
            composer dumpautoload --optimize
            composer dumpautoload --classmap-authoritative

         */

        if ($this->option('clear')) {
            $bar = $this->output->createProgressBar(4);

            Artisan::call('cache:clear');
            $bar->advance();
            Artisan::call('config:clear');
            $bar->advance();
            Artisan::call('route:clear');
            $bar->advance();
            Artisan::call('optimize'); 

        } else {
            $bar = $this->output->createProgressBar(4);

            Artisan::call('cache:clear');
            $bar->advance();

            /*Artisan::call('route:cache');
            $bar->advance();*/

            Artisan::call('optimize'); 

            Artisan::call('config:cache');
            $bar->advance();

            Artisan::call('api:cache');
            $bar->advance();

        }


        $bar->finish();
        
        $this->info('Optimization DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
