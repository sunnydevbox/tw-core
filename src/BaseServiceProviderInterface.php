<?php
namespace Sunnydevbox\TWCore;

interface BaseServiceProviderInterface 
{
	public function registerProviders();
	
	public function registerCommands();

	public function registerPublishers();
}