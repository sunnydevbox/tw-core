<?php

return [
	'tables' => [
		'activity_log' => 'activity_log',
	],
	'controllers' => [
		'activity_log' => Sunnydevbox\TWCore\Http\Controllers\ActivityLogController::class,
	],
	'models' => [
		'activity_log' => Sunnydevbox\TWCore\Models\ActivityLog::class,
	],
	'repositories' => [
		'activity_log' => Sunnydevbox\TWCore\Repositories\ActivityLogRepositories::class,
	],


	'enable_log_viewer' => true,
];