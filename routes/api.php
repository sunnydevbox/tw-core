<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) 
{

	$api = app('Dingo\Api\Routing\Router');



	$api->version('v1', ['middleware' => []], function ($api) {
		
		// $api->post('auth', [
		// 	'as' => 'auth',
		// 	'uses' => '\Sunnydevbox\TWCore\Http\Controllers\AuthenticateController@authenticate',
		// ]);


		// $api->get('auth/token', [
		// 	'as' => 'auth.token',
		// 	'uses' => '\Sunnydevbox\TWCore\Http\Controllers\AuthenticateController@getAuthenticatedUser'

		// ]);
		
		/** ADD THIS TO THE PARENT CLASS */
		// $api->get('suppdata', 						'\Sunnydevbox\TWCore\Http\Controllers\SuppDataController@index');

		$api->resource('activity-logs', config('tw-core.controllers.activity_log'),  ['except' => ['create', 'store', 'update', 'destroy']]);
	});

}